import os
from bs4 import BeautifulSoup

import requests


def get_auth_token(logger) -> str:
    """
    Получает токен аутентификации от сервера аутентификации.
    """
    url = "https://auth.alpha.kapi.bar/v1/user/login/"
    credentials = {
        "username": f"{os.getenv('LOGIN_KAP')}",
        "password": f"{os.getenv('PASS_KAP')}"
    }
    try:
        response = requests.post(url, json=credentials)
        response.raise_for_status()
        return response.json().get("access_token", "")
    except requests.RequestException as e:
        logger.error(f"Ошибка при получении токена аутентификации: {e}")
        return ""


def sent_letter(nickname: str, logger) -> bool:
    """
    Отправляет запрос на активацию пользователя.

    :param nickname: Имя пользователя для активации.
    :return: Возвращает True, если активация прошла успешно, иначе False.
    """
    token = get_auth_token(logger)
    if not token:
        return False

    url = "https://auth.alpha.kapi.bar/v1/user/activate/"
    headers = {
        "Authorization": f"Bearer {token}",
        "Content-Type": "application/x-www-form-urlencoded"
    }
    data = {
        "username": nickname
    }

    try:
        response = requests.post(url, headers=headers, data=data)
        return response.status_code == 200
    except requests.RequestException as e:
        logger.error(f"Ошибка при активации пользователя: {e}")
        return False


def check_exist_nick_kapibara(nickname: str, logger) -> bool:
    """
    Проверяет наличие профиля пользователя на сайте alpha.kapi.bar.

    :param nickname: Никнейм пользователя для проверки.
    :return: Возвращает False, если профиль существует, иначе True.
    """
    url = f"https://backend.alpha.kapi.bar/v1/users/{nickname}"
    try:
        response = requests.get(url)
        logger.info(f"Response code kapibara: {response.status_code}")
        return response.status_code == 200
    except requests.RequestException as e:
        # В случае ошибки запроса, лучше записать её в лог и вернуть True
        logger.error(f"Ошибка при запросе к alpha.kapi.bar: {e}")
        return False


def check_exist_nick_pikabu(nickname: str, logger) -> bool:
    """
    Проверяет наличие профиля пользователя на сайте Pikabu.

    :param nickname: Никнейм пользователя для проверки.
    :return: Возвращает True, если профиль существует, иначе False.
    """
    url = f"https://pikabu.ru/@{nickname}"
    try:
        response = requests.get(url)
        # Если статус-код ответа 200, профиль существует
        logger.info(f"Response code pikabu: {response.status_code}")
        return response.status_code == 200
    except requests.RequestException as e:
        # В случае ошибки запроса, лучше записать её в лог и вернуть False
        logger.error(f"Ошибка при запросе к Pikabu: {e}")
        return False


def check_comment_on_pikabu(url: str, nickname: str, code: str, logger) -> bool:
    """
    Проверяет наличие профиля пользователя на сайте Pikabu.

    :param nickname: Никнейм пользователя для проверки.
    :return: Возвращает True, если профиль существует, иначе False.
    """
    try:
        response = requests.get(url)
        print(f"Response code: {response.status_code}")
        if response.status_code == 200:
            soup = BeautifulSoup(response.text, 'html.parser')

            nickname_element = soup.find('div', class_='comment__user')
            nick = nickname_element.attrs.get('data-name')
            comment_element = soup.find('div', class_='comment__content')
            comment_text = comment_element.text.strip() if comment_element else ""

            if nick.lower() == nickname.lower() and str(code) in comment_text:
                print(f"Ник: {nickname}, Комментарий: {comment_text}")
                return True
            else:
                print("Ник или комментарий не найдены.")
                return False
        else:
            return False
    except requests.RequestException as e:
        print(f"Ошибка при запросе к Pikabu: {e}")
        return False
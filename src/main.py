import logging
import os
from datetime import datetime, timedelta
from logging.handlers import RotatingFileHandler
import random
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, MessageHandler, Filters, CallbackContext

from const_message import (hello, disc_1, decline_answer, request_new_nickname,
                           nickname_not_available, response_big_problem, congrats,
                           we_have_a_problem, sended_letter, request_pikabu_nickname,
                           response_pnick_exist, nickname_available_new)
# Настройка ротационного логирования
from utils import sent_letter, check_exist_nick_kapibara, check_exist_nick_pikabu, check_comment_on_pikabu

log_file = '/app/logs/bot_log.log'
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = RotatingFileHandler(log_file, maxBytes=5 * 1024 * 1024,
                              backupCount=2)  # Размер файла 5 МБ, сохраняем два архива
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

# Telegram Bot Token
TOKEN = os.getenv('TOKEN')
CHECK_CHANNEL_NAME = os.getenv("CHECK_CHANNEL_NAME")


def check_subscription(context: CallbackContext) -> bool:
    """
    Проверяет подписку пользователя на заданный канал.

    :param context: Контекст вызова, предоставляемый python-telegram-bot.
    :return: Возвращает True, если пользователь подписан, иначе False.
    """
    user_id = context.user_data.get('user_id')
    try:
        chat_member = context.bot.get_chat_member(chat_id=CHECK_CHANNEL_NAME, user_id=user_id)
        is_subscribed = chat_member.status not in ['left', 'kicked']
        logger.info(f"Subscription check for user {user_id}: {is_subscribed}")
        return is_subscribed
    except Exception as e:
        logger.error(f"Error checking subscription for user {user_id}: {e}")
        return False


def start(update: Update, context: CallbackContext) -> None:
    """
    Обрабатывает команду /start. Отправляет пользователю приветственное сообщение и опции выбора.

    :param update: Объект Update, предоставляемый python-telegram-bot.
    :param context: Контекст вызова, предоставляемый python-telegram-bot.
    """
    username = update.effective_user.first_name
    user_id = update.message.from_user.id
    context.user_data['user_id'] = user_id

    message = hello.replace('@имя_пользователя', f"{username}") + disc_1
    logger.info(f"User {user_id} started the bot.")

    keyboard = [
        [InlineKeyboardButton("Согласен", callback_data='agree1'),
         InlineKeyboardButton("Не согласен", callback_data='decline1')]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text(message, reply_markup=reply_markup)


def button(update: Update, context: CallbackContext) -> None:
    """
    Обрабатывает нажатия на кнопки Inline-клавиатуры.

    :param update: Объект Update, предоставляемый python-telegram-bot.
    :param context: Контекст вызова, предоставляемый python-telegram-bot.
    """
    query = update.callback_query
    query.answer()
    user_id = context.user_data.get('user_id')
    logger.info(f"User {user_id} clicked button: {query.data}")

    if query.data == 'decline1':
        query.message.reply_text(decline_answer)

    elif query.data == 'agree1':
        if check_subscription(context):
            choose_nick(update, context)
        else:
            query.message.reply_text(
                f"Пожалуйста, для продолжения регистрации отправьте заявку в группу "
                f"https://t.me/alpha_kapibara и дождитесь вступления, после чего повторно начните с "
                f"команды /start")

    elif query.data == 'new_nick' or query.data == 'transfer_nick':
        handle_nick_choice(update, context)

    elif query.data == 'problems':
        query.message.reply_text(response_big_problem)
    elif query.data == 'ready':
        sent_user_check_letter(update, context)
    elif query.data == 'onborded':
        query.message.reply_text(congrats)


def choose_nick(update: Update, context: CallbackContext) -> None:
    """
    Предлагает пользователю выбор между новым ником и переносом ника с Пикабу.

    :param update: Объект Update, предоставляемый python-telegram-bot.
    :param context: Контекст вызова, предоставляемый python-telegram-bot.
    """
    user_id = context.user_data.get('user_id')
    logger.info(f"User {user_id} is choosing a nickname option.")

    keyboard = [
        [InlineKeyboardButton("Новый ник", callback_data='new_nick'),
         InlineKeyboardButton("Перенести ник с Пикабу", callback_data='transfer_nick')]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    update.callback_query.message.reply_text(
        "Отлично. Будете использовать использовать ник с пикабу или создашь себе новый?", reply_markup=reply_markup)


def handle_comment_link_input(update: Update, context: CallbackContext) -> None:
    """
    Обрабатывает ввод ссылки на комментарий пользователя.

    Эта функция вызывается, когда пользователь отправляет текстовое сообщение,
    и предполагается, что это ссылка на комментарий в Пикабу.

    :param update: Объект Update от python-telegram-bot, содержащий информацию о сообщении пользователя.
    :param context: Контекст вызова от python-telegram-bot, содержащий данные о состоянии диалога и пользователя.
    :return: None. Функция не возвращает значения, но отправляет ответ пользователю через Telegram API.
    """
    user_input = update.message.text
    user_id = update.message.from_user.id  # Получаем ID пользователя

    # Логирование получения ссылки от пользователя
    logger.info(f"User {user_id} submitted comment link: {user_input}")

    # Проверка комментария на Пикабу
    if check_comment_on_pikabu(
            url=user_input,
            nickname=context.user_data.get('choised_nick'),
            code=context.user_data.get('verification_number'),
            logger=logger
    ):
        sent_user_go_register_kapibar(update, context)
        context.user_data['waiting_for_comment_link'] = False  # Сброс флага ожидания
        logger.info(f"Comment link verified for user {user_id}. Registration proceeding.")
    else:
        update.message.reply_text(
            "Комментарий на Пикабу не найден. Пожалуйста, отправьте корректную ссылку на комментарий.")
        logger.warning(f"Invalid comment link submitted by user {user_id}.")


def handle_nick_choice(update: Update, context: CallbackContext) -> None:
    """
    Обрабатывает выбор пользователя между новым ником и переносом ника с Пикабу.

    :param update: Объект Update, предоставляемый python-telegram-bot.
    :param context: Контекст вызова, предоставляемый python-telegram-bot.
    """
    query = update.callback_query
    user_id = context.user_data.get('user_id')
    logger.info(f"User {user_id} made a choice: {query.data}")

    query = update.callback_query
    query.answer()
    if query.data == 'new_nick':
        update.callback_query.message.reply_text(request_new_nickname)
        context.user_data['nick_choice'] = 'new'
    elif query.data == 'transfer_nick':
        update.callback_query.message.reply_text(request_pikabu_nickname)
        context.user_data['nick_choice'] = 'transfer'
        context.user_data['waiting_for_comment_link'] = False  # Сброс флага





def sent_user_check_letter(update: Update, context: CallbackContext) -> None:
    """
    Отправляет пользователю сообщение о проверке регистрационного письма.

    :param update: Объект Update, предоставляемый python-telegram-bot.
    :param context: Контекст вызова, предоставляемый python-telegram-bot.
    """
    user_id = context.user_data.get('user_id')
    logger.info(f"User {user_id} is being asked to check the letter.")

    query = update.callback_query
    keyboard = [
        [InlineKeyboardButton("Я на сайте", callback_data='onborded'),
         InlineKeyboardButton("Есть сложности", callback_data='problems')]
    ]
    try:
        if sent_letter(context.user_data.get('choised_nick'), logger):
            reply_markup = InlineKeyboardMarkup(keyboard)
            query.message.reply_text(
                sended_letter, reply_markup=reply_markup)
        else:
            query.message.reply_text(we_have_a_problem)
    except:
        query.message.reply_text(we_have_a_problem)


def sent_user_go_register_kapibar(update: Update, context: CallbackContext) -> None:
    """
    Предлагает пользователю перейти к регистрации на сайте.

    :param update: Объект Update, предоставляемый python-telegram-bot.
    :param context: Контекст вызова, предоставляемый python-telegram-bot.
    """
    user_id = context.user_data.get('user_id')
    logger.info(f"User {user_id} is being asked to register on the site.")

    keyboard = [
        [InlineKeyboardButton("Пришлите мне письмо", callback_data='ready'),
         InlineKeyboardButton("Есть сложности", callback_data='problems')]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text(
        nickname_available_new.format(nickname=context.user_data.get('choised_nick')),
        reply_markup=reply_markup)


def handle_nick_input(update: Update, context: CallbackContext) -> None:
    """
    Обрабатывает ввод ника пользователя.

    Эта функция вызывается, когда пользователь отправляет текстовое сообщение, содержащее ник.
    В зависимости от выбора пользователя (новый ник или перенос существующего с Пикабу),
    функция выполняет разные действия.

    :param update: Объект Update от python-telegram-bot, содержащий информацию о сообщении пользователя.
    :param context: Контекст вызова от python-telegram-bot, содержащий данные о состоянии диалога и пользователя.
    """
    user_input = update.message.text
    user_id = update.message.from_user.id
    try_count = context.user_data.get('try_count', 0)

    # Логирование ввода ника пользователя
    logger.info(f"User {user_id} submitted nickname: {user_input}")

    # Проверка времени для новой попытки
    last_try_time = context.user_data.get('last_try_time')
    if last_try_time and datetime.now() - last_try_time < timedelta(seconds=5):
        update.message.reply_text("Пожалуйста, подождите 5 минут перед следующей попыткой.")
        logger.info(f"User {user_id} has to wait before next attempt.")
        return

    nick_choice = context.user_data.get('nick_choice')
    if not nick_choice:
        update.message.reply_text(
            f"Пожалуйста, если в прошлый раз что-то пошло не так "
            f"и вы начали процесс регистрации заново - начните с команды /start")
        logger.warning(f"User {user_id} has not made a nickname choice yet.")
        return
    if nick_choice == 'new':
        exist_kap = check_exist_nick_kapibara(user_input, logger)
        exist_pik = check_exist_nick_pikabu(user_input, logger)
        logger.info(f"User {user_id} is checking availability for new nickname: {user_input}")

        if not exist_kap and not exist_pik:
            context.user_data['choised_nick'] = user_input
            sent_user_go_register_kapibar(update, context)
            logger.info(f"User {user_id}'s chosen new nickname '{user_input}' is available.")
            return
        else:
            resource_na = 'пикабу' if not exist_pik else 'Капибаре'
            error_message = nickname_not_available.format(resource=resource_na)
            update.message.reply_text(error_message)
            logger.warning(f"User {user_id}'s chosen new nickname '{user_input}' is not available on {resource_na}.")
            return

    elif nick_choice == 'transfer':
        if context.user_data.get('waiting_for_comment_link'):
            handle_comment_link_input(update, context)  # Обрабатываем как ссылку на комментарий
            logger.info(f"User {user_id} is transferring nickname, waiting for comment link.")
            return

        exist_kap = check_exist_nick_kapibara(user_input, logger)
        exist_pik = check_exist_nick_pikabu(user_input, logger)
        logger.info(f"User {user_id} is checking availability for transferring nickname: {user_input}")

        if not exist_kap and exist_pik:
            link = f'https://pikabu.ru/@{user_input}'
            context.user_data['choised_nick'] = user_input
            context.user_data['waiting_for_comment_link'] = True

            context.user_data['verification_number'] = random.randint(10000, 99999)

            update.message.reply_text(
                response_pnick_exist.format(
                    link=link,
                    code=context.user_data['verification_number'],
                    nick_name=user_input
                ))
            logger.info(f"User {user_id} can transfer nickname '{user_input}'. Verification initiated.")
            return
        else:
            if exist_kap:
                update.message.reply_text(nickname_not_available.format(resource='Капибаре'))
                logger.warning(f"Nickname '{user_input}' is not available on Kapibara for user {user_id}.")

            if not exist_pik:
                update.message.reply_text(f"Ник {user_input} не найден на пикабу.")
                logger.warning(f"Nickname '{user_input}' not found on Pikabu for user {user_id}.")

    context.user_data['try_count'] = try_count + 1
    if try_count >= 3:  # Проверяем, было ли это третьей попыткой
        update.message.reply_text(
            "Превышено количество попыток. Пожалуйста, подождите 5 минут перед следующей попыткой.")
        context.user_data['last_try_time'] = datetime.now()
        context.user_data['try_count'] = 0  # Сбрасываем счетчик попыток
        logger.warning(f"User {user_id} exceeded maximum number of attempts for nickname input.")
        return
    else:
        context.user_data['try_count'] = try_count + 1
        update.message.reply_text(f"Попытка {try_count + 1}/3. Пожалуйста, введите ник еще раз:")
        logger.info(f"User {user_id} is on attempt {try_count + 1} for nickname input.")


def main() -> None:
    updater = Updater(TOKEN, use_context=True)
    dp = updater.dispatcher

    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CallbackQueryHandler(button))
    # dp.add_handler(CallbackQueryHandler(button_new_exist))
    dp.add_handler(MessageHandler(Filters.text & ~Filters.command, handle_nick_input))
    dp.add_handler(MessageHandler(Filters.text & ~Filters.command, handle_comment_link_input))

    # dp.add_error_handler(error)

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
